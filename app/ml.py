import numpy as np
import random


class NeuralNetwork:

    def __init__(self):
        # seeding for random number generation
        # np.random.seed(1)
        np.random.seed(random.randint(1, 4294967295))
        # converting weights to a 3 by 1 matrix with values from -1 to 1 and mean of 0
        self.synaptic_weights = 2 * np.random.random((9, 9)) - 1

    @staticmethod
    def sigmoid(x):
        # applying the sigmoid function
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def sigmoid_derivative(x):
        # computing derivative to the Sigmoid function
        return x * (1 - x)

    def think(self, inputs):
        # passing the inputs via the neuron to get output
        # converting values to floats

        output = self.sigmoid(np.dot(inputs, self.synaptic_weights))
        return output

    def fields_to_layers(self, fields, player):
        inputs = list()
        for y in range(0, 3):
            for x in range(0, 3):
                if fields[x][y] == player:
                    val = 10000
                elif fields[x][y] == 0:
                    val = 0
                else:
                    val = -10000

                inputs.append(self.sigmoid(val))

        return inputs

    def choose_clicked_field(self, fields_values):
        return np.argmax(fields_values)
        return fields_values.index(max(fields_values))
