# config

left = 100
top = 100
cell_size = 100
padding = 12

field_color = (0, 0, 255)
x_color = (255, 255, 0)
circle_color = (255, 0, 255)
width = 3
