import pygame
import config
import math
from app import ml


class Game:

    def __init__(self):
        pygame.init()
        pygame.font.init()
        self.myfont = pygame.font.SysFont('Comic Sans MS', 30)
        self.screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption('Tic Tac Toe')
        self.clock = pygame.time.Clock()
        self.crashed = False
        self.player = 0
        self.score = [0, 0]
        self.fields = None  # TBA
        self.new_round()

    def new_round(self):
        self.fields = [[0] * 3, [0] * 3, [0] * 3]
        self.draw()

    @staticmethod
    def mouse_to_pos(pos):
        x = (pos[0] - config.left) / config.cell_size
        y = (pos[1] - config.top) / config.cell_size
        return math.floor(x), math.floor(y)

    @staticmethod
    def check_in_field(pos):
        return 0 <= pos[0] < 3 and 0 <= pos[1] < 3

    def draw_score(self):
        text_surface = self.myfont.render(f'Player1: {self.score[0]} - Player2: {self.score[1]}', False,
                                          (255, 234, 255))
        self.screen.blit(text_surface, (0, 0))

    def draw_x(self, x, y):
        pygame.draw.line(self.screen, config.x_color,
                         (config.left + x * config.cell_size + config.padding,
                          config.top + (y + 1) * config.cell_size - config.padding),
                         (config.left + (x + 1) * config.cell_size - config.padding,
                          config.top + y * config.cell_size + config.padding),
                         config.width)
        pygame.draw.line(self.screen, config.x_color,
                         (config.left + (x + 1) * config.cell_size - config.padding,
                          config.top + (y + 1) * config.cell_size - config.padding),
                         (config.left + x * config.cell_size + config.padding,
                          config.top + y * config.cell_size + config.padding),
                         config.width)

    def draw_circle(self, x, y):
        pygame.draw.circle(self.screen, config.circle_color,
                           (int(config.left + (x + 0.5) * config.cell_size),
                            int(config.top + (y + 0.5) * config.cell_size)),
                           int(config.cell_size / 2 - config.padding),
                           config.width)

    def draw_field(self):
        blue = (0, 0, 255)
        for x in range(0, 4):
            pygame.draw.line(self.screen, blue,
                             (config.left + x * config.cell_size, config.top + 0 * config.cell_size),
                             (config.left + x * config.cell_size, config.top + 3 * config.cell_size))
        for y in range(0, 4):
            pygame.draw.line(self.screen, blue,
                             (config.left + 0 * config.cell_size, config.top + y * config.cell_size),
                             (config.left + 3 * config.cell_size, config.top + y * config.cell_size))

    def draw(self):
        black = (0, 0, 0)
        self.screen.fill(black)
        self.draw_score()
        self.draw_field()
        for y in range(0, 3):
            for x in range(0, 3):
                if self.fields[x][y] == 1:
                    self.draw_x(x, y)
                if self.fields[x][y] == 2:
                    self.draw_circle(x, y)

    def check_winner(self):
        combinations = (
            (0, 0, 1, 0),
            (0, 1, 1, 0),
            (0, 2, 1, 0),
            (0, 0, 0, 1),
            (1, 0, 0, 1),
            (2, 0, 0, 1),
            (0, 0, 1, 1),
            (2, 0, -1, 1),
        )

        for (x, y, dx, dy) in combinations:
            pnum = {0: 0, 1: 0, 2: 0}
            for i in range(3):
                pnum[self.fields[x][y]] += 1
                x += dx
                y += dy
            if pnum[1] == 3:
                return 1
            if pnum[2] == 3:
                return 2

    def check_full_field(self):
        for x in range(3):
            for y in range(3):
                if self.fields[x][y] == 0:
                    return False
        return True

    def run(self):

        nn = ml.NeuralNetwork()

        while not self.crashed:
            for event in pygame.event.get():
                # if event.key
                if event.type == pygame.QUIT:
                    self.crashed = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.crashed = True
                if event.type == pygame.MOUSEBUTTONDOWN:

                    field_pos = self.mouse_to_pos(event.pos)
                    if self.check_in_field(field_pos):
                        if self.fields[field_pos[0]][field_pos[1]] == 0:
                            self.fields[field_pos[0]][field_pos[1]] = self.player + 1
                            self.player = (self.player + 1) % 2
                            self.draw()

                            winner = self.check_winner()
                            if winner:
                                self.score[winner - 1] += 1
                                print(f"Winner {winner}")
                                self.new_round()

                            if self.check_full_field():
                                self.new_round()

                    print(self.player)
                    print(self.fields)
                    inputs = nn.fields_to_layers(self.fields, 1 if self.player == 1 else 2)

                    print(inputs)
                    outputs = nn.think(inputs)
                    print(outputs)
                    print(nn.choose_clicked_field(outputs))
                    pygame.display.update()



            self.clock.tick(20)
            print()

        pygame.quit()
